<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 8:27
 */

namespace EightBitGroup\GeoBundle\Client;


use EightBitGroup\GeoBundle\Entity\Location;
use EightBitGroup\GeoBundle\Entity\LocationCollection;

interface GeoClientInterface
{
    /**
     * @return Location[]|LocationCollection
     */
    public function receiveLocations(): LocationCollection;
}