<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 16:43
 */

namespace EightBitGroup\GeoBundle\Client;


use EightBitGroup\GeoBundle\Entity\Location;
use EightBitGroup\GeoBundle\Exception\Client\GeoClientException;
use EightBitGroup\GeoBundle\Exception\Client\ResponseParserException;

class ResponseParser
{
    /**
     * @param array $decoded
     * @return array(string, string) data and type of data
     * @throws ResponseParserException
     */
    public function parse(array $decoded): array
    {
        if (!$this->hasRequiredFields($decoded)) {
            throw new ResponseParserException();
        }

        $data    = $decoded['data'];
        $success = filter_var($decoded['success'], FILTER_VALIDATE_BOOLEAN);

        if ($success === false && !$this->hasError($data)) {
            throw new ResponseParserException();
        }

        if ($this->hasError($data)) {
            return [$data, GeoClientException::class];
        }

        if (!$this->hasLocations($data)) {
            throw new ResponseParserException();
        }

        return [$data['locations'], 'array<'.Location::class.'>'];
    }


    private function hasRequiredFields(array $decoded): bool
    {
        return !isset($decoded['data']) || !isset($decoded['status']);
    }


    private function hasError(array $data): bool
    {
        return isset($data['message']) && isset($data['code']);
    }


    private function hasLocations(array $data): bool
    {
        return isset($data['locations']);
    }
}