<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 8:27
 */

namespace EightBitGroup\GeoBundle\Client;


use EightBitGroup\GeoBundle\Entity\LocationCollection;
use EightBitGroup\GeoBundle\Exception\Client\GeoClientException;
use EightBitGroup\GeoBundle\Exception\Client\ResponseParserException;
use EightBitGroup\GeoBundle\Transport\TransportInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GeoClient implements GeoClientInterface
{
    /**
     * @var TransportInterface
     */
    private $transport;
    /**
     * @var ResponseParser
     */
    private $parser;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ValidatorInterface
     */
    private $validator;


    public function __construct(
        TransportInterface  $transport,
        ResponseParser      $parser,
        SerializerInterface $serializer,
        ValidatorInterface  $validator
    )
    {
        $this->transport  = $transport;
        $this->parser     = $parser;
        $this->serializer = $serializer;
        $this->validator  = $validator;
    }


    public function receiveLocations(): LocationCollection
    {
        $body    = $this->transport->get();
        $decoded = $this->jsonDecode($body);
        
        list($parsed, $type) = $this->parser->parse($decoded);

        $json = json_encode($parsed);
        $data = $this->serializer->deserialize($json, $type, 'json');

        if ($data instanceof \Exception) {
            throw $data;
        }

        $locations = new LocationCollection($data);
        $errors    = $this->validator->validate($locations);

        if ($errors->count() > 0) {
            throw new GeoClientException((string) $errors);
        }

        return $locations;
    }


    private function jsonDecode(string $body): array 
    {
        $decoded = json_decode($body, true);
        
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new ResponseParserException(json_last_error_msg());
        }
        
        return $decoded;
    }
}