<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 16:43
 */

namespace EightBitGroup\GeoBundle\Tests\Client;


use EightBitGroup\GeoBundle\Client\ResponseParser;
use EightBitGroup\GeoBundle\Entity\Location;
use EightBitGroup\GeoBundle\Exception\Client\GeoClientException;
use EightBitGroup\GeoBundle\Exception\Client\ResponseParserException;
use EightBitGroup\GeoBundle\Tests\Client\ResponseFixture;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ResponseParserTest extends KernelTestCase
{
    use ResponseFixture;


    public function testParse()
    {
        $decoded  = json_decode($this->getSuccessBody(), true);

        list($parsed, $type) = $this->parser()->parse($decoded);

        $this->assertEquals($decoded['data']['locations'], $parsed);
        $this->assertEquals('array<'.Location::class.'>', $type);
    }


    public function testParseError()
    {
        $decoded  = json_decode($this->getErrorBody(), true);

        list($parsed, $type) = $this->parser()->parse($decoded);

        $this->assertEquals($decoded['data'], $parsed);
        $this->assertEquals(GeoClientException::class, $type);
    }


    public function testParseUndefinedFormat()
    {
        $decoded  = json_decode($this->getUndefinedFormatBody(), true);
        $this->setExpectedException(ResponseParserException::class);

        $this->parser()->parse($decoded);
    }


    private function parser(): ResponseParser
    {
        return static::$kernel->getContainer()->get('eight_bit_group_geo.client.response_parser');
    }


    protected function setUp()
    {
        static::bootKernel();
    }
}