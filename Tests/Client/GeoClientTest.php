<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 9:37
 */

namespace EightBitGroup\GeoBundle\Tests\Client;


use EightBitGroup\GeoBundle\Client\GeoClientInterface;
use EightBitGroup\GeoBundle\Entity\LocationCollection;
use EightBitGroup\GeoBundle\Exception\Client\GeoClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException as HttpErrorException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class GeoClientTest extends KernelTestCase
{
    use ResponseFixture;


    public function testSuccessReceiveLocations()
    {
        $this->setupResponse(200, $this->getSuccessBody());

        $locations = $this->client()->receiveLocations();

        $this->assertInstanceOf(LocationCollection::class, $locations);
        $this->assertCount(2, $locations);

        $this->assertEquals('Eiffel Tower', $locations[0]->getName());
        $this->assertEquals(21.12, $locations[0]->getCoordinates()->getLat(), 0.000001);
        $this->assertEquals(19.56, $locations[0]->getCoordinates()->getLong(), 0.000001);

        $this->assertEquals('Moscow', $locations[1]->getName());
        $this->assertEquals(54.45, $locations[1]->getCoordinates()->getLat(), 0.000001);
        $this->assertEquals(37.37, $locations[1]->getCoordinates()->getLong(), 0.000001);
    }


    public function testReceiveLocationsWithError()
    {
        $this->setupResponse(200, $this->getErrorBody());
        $this->setExpectedException(GeoClientException::class, 'error message', 42);

        $this->client()->receiveLocations();
    }


    public function testReceiveLocationsWithHttpError()
    {
        $this->setupResponse(403, $this->getErrorBody());
        $this->setExpectedException(HttpErrorException::class, 'Forbidden', 403);

        $this->client()->receiveLocations();
    }


    public function testReceiveLocationsWithInvalidData()
    {
        $this->setupResponse(200, $this->getInvalidBody());
        $this->setExpectedException(GeoClientException::class);

        $this->client()->receiveLocations();
    }


    private function setupResponse(int $code, string $body)
    {
        $mock = new MockHandler([
            new Response($code, [], $body)
        ]);

        $handler = HandlerStack::create($mock);
        $guzzle  = new Client(['handler' => $handler]);

        static::$kernel->getContainer()->set('eight_bit_group_geo.guzzle_http.client', $guzzle);
    }


    private function client(): GeoClientInterface
    {
        return static::$kernel->getContainer()->get('eight_bit_group_geo.client');
    }


    protected function setUp()
    {
        static::bootKernel();
    }
}
