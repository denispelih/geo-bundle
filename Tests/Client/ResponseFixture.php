<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 17:06
 */

namespace EightBitGroup\GeoBundle\Tests\Client;


trait ResponseFixture
{
    private function getSuccessBody(): string
    {
        return
<<<JSON
{
    "data": {
        "locations": [
            {
                "name": "Eiffel Tower",
                "coordinates": {
                    "lat": 21.12,
                    "long": 19.56
                }
            },
            {
                "name": "Moscow",
                "coordinates": {
                    "lat": 54.45,
                    "long": 37.37
                }
            }
        ]
    },
    "success": true
}
JSON;
    }


    private function getErrorBody(): string
    {
        return
<<<JSON
{
    "data": {
        "message": "error message",
        "code": 42
    },
    "success": false
}
JSON;
    }


    private function getInvalidBody(): string
    {
        return
 <<<JSON
{
    "data": {
        "locations": [
            {
                "name": "Eiffel Tower",
                "coordinates": {
                    "lat": "invalid latitude",
                    "long": 19.56
                }
            }
        ]
    },
    "success": true
}
JSON;
    }


    private function getUndefinedFormatBody(): string
    {
        return
<<<JSON
{
    "data": {
        "_": [
            {
                "name": "Eiffel Tower",
                "coordinates": {
                    "lat": "invalid latitude",
                    "long": 19.56
                }
            }
        ]
    },
    "success": true
}
JSON;
    }
}