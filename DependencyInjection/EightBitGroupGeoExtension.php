<?php

namespace EightBitGroup\GeoBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class EightBitGroupGeoExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $clientDefinition = $container->findDefinition($config['client']);
        $container->setDefinition('eight_bit_group_geo.client', $clientDefinition);

        $transportDefinition = $container->findDefinition($config['transport']);
        $transportDefinition->setMethodCalls([
            ['setUri', [ $config['uri'] ]]
        ]);
        $container->setDefinition('eight_bit_group_geo.transport', $transportDefinition);
    }
}
