<?php

namespace EightBitGroup\GeoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('eight_bit_group_geo');

        $rootNode
            ->children()
                ->scalarNode('uri')->isRequired()->end()
                ->scalarNode('client')->defaultValue('eight_bit_group_geo.client')->end()
                ->scalarNode('transport')->defaultValue('eight_bit_group_geo.transport')->end()
            ->end();

        return $treeBuilder;
    }
}
