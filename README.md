Test task for PHP-developer in [8BitGroup](http://8bitgroup.ru/)
=======================================================

Description
------------
Need to implement Symfony bundle for getting JSON-encoded locations data stored in predefined format.

### Acceptance criteria
1. Client should be defined as a service class in a bundle config;
2. Client should utilize CURL as a transport layer (can rely upon any third-party bundle however should be implemented as a separate class/package);
3. Properly defined exceptions should be thrown on CURL errors, malformed JSON response and error JSON response;
4. Resulting data should be fetched as an array (or other collection) of properly defined PHP objects.

### JSON response format
```json
{
    "data": {
        "locations": [
            {
                "name": "Eiffel Tower",
                "coordinates": {
                    "lat": 21.12,
                    "long": 19.56
                }
            },
            {
                "name": "Moscow",
                "coordinates": {
                    "lat": 54.45,
                    "long": 37.37
                }
            }
        ]
    },
    "success": true
}
```
### JSON error response format
```json
{
    "data": {
        "message": "error message",
        "code": 42
    },
    "success": false
}
```

Requirements
------------
* PHP 7
* Symfony 3.0

Installation
------------

Use [Composer](http://getcomposer.org/) to install this bundle.

Add a new repository to your *composer.json* file:
```json
"repositories": [
        {
            "type":"git",
            "url":"git@bitbucket.org:denispelih/geo-bundle.git"
        }
    ]
```

Run in console:
```sh
composer require denispelih/geo-bundle:dev-master
```

Add the bundle to the application kernel:

```php
        $bundles = array(
            // ...
            new EightBitGroup\GeoBundle\EightBitGroupGeoBundle(),
        );
```

Configuration
-------------

```yaml
# app/config/config.yml
eight_bit_group_geo:
    # The uri which stored JSON
    uri: ~
    # Service of client
    client: eight_bit_group_geo.client
    # Service of HTTP-transport 
    transport: eight_bit_group_geo.transport
```

Usage
-----

```php
// Get an instance of the client as a service 
$geo = $this->get('eight_bit_group_geo.client');
// And call method receiveLocations()
// which will return an instance of  \EightBitGroup\GeoBundle\Entity\LocationCollection
$locations = $geo->receiveLocations();
```

Credits
-------

This bundle has been wrote by [Denis Pelih](mailto:denispelih@gmail.com).
