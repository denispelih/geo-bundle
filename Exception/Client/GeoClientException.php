<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 14:58
 */

namespace EightBitGroup\GeoBundle\Exception\Client;


use JMS\Serializer\Annotation as JMS;

class GeoClientException extends \Exception
{
    /**
     * @JMS\Type("integer")
     */
    protected $code    = -1;
    /**
     * @JMS\Type("string")
     */
    protected $message = 'Undefined error';
}