<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 15:37
 */

namespace EightBitGroup\GeoBundle\Exception\Client;


class ResponseParserException extends \Exception
{
    protected $message = 'Undefined format of response';
}