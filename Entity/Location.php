<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 12:00
 */

namespace EightBitGroup\GeoBundle\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

class Location
{
    /**
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @Assert\NotBlank
     * @Assert\Type("object")
     * @Assert\Valid
     * @JMS\Type("EightBitGroup\GeoBundle\Entity\Coordinates")
     */
    private $coordinates;


    public function getName(): string
    {
        return $this->name;
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }


    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }


    public function setCoordinates(Coordinates $coordinates)
    {
        $this->coordinates = $coordinates;
    }

}