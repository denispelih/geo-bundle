<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 12:01
 */

namespace EightBitGroup\GeoBundle\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

class Coordinates
{
    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @JMS\Type("coordinate")
     */
    private $lat;

    /**
     * @Assert\NotBlank
     * @Assert\Type("float")
     * @JMS\Type("coordinate")
     */
    private $long;


    public function getLat()
    {
        return $this->lat;
    }


    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }


    public function getLong()
    {
        return $this->long;
    }


    public function setLong(float $long)
    {
        $this->long = $long;
    }
}