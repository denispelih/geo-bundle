<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 8:36
 */

namespace EightBitGroup\GeoBundle\Transport;


interface TransportInterface
{
    public function get(): string;
    public function setUri(string $uri);
}