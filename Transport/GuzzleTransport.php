<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 24.02.16
 * Time: 8:39
 */

namespace EightBitGroup\GeoBundle\Transport;


use GuzzleHttp\ClientInterface;

class GuzzleTransport implements TransportInterface
{
    private $uri;
    private $guzzle;


    public function __construct(ClientInterface $guzzle)
    {
        $this->guzzle = $guzzle;
    }


    public function get(): string
    {
        $response = $this->guzzle->request('GET', $this->uri);
        $body     = $response->getBody();

        return (string) $body;
    }


    public function setUri(string $uri)
    {
        $this->uri = $uri;
    }
}