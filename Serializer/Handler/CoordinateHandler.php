<?php
/**
 * Created by PhpStorm.
 * User: denispelih
 * Date: 25.02.16
 * Time: 6:05
 */

namespace EightBitGroup\GeoBundle\Serializer\Handler;


use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\VisitorInterface;

/**
 * By default if I define @Type("float") and coordinate will be a string "hello" in JSON,
 * it's deserialized and casted to a (double) 0. But this coordinate is not valid. It must be NULL!
 * https://github.com/schmittjoh/serializer/pull/507#issuecomment-153029341
 */
class CoordinateHandler implements SubscribingHandlerInterface
{
    public static function getSubscribingMethods(): array
    {
        $formats = ['json', 'xml', 'yml'];
        $methods = [];

        foreach ($formats as $format) {
            $methods[] = [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'type'      => 'coordinate',
                'format'    => $format,
                'method'    => 'deserializeCoordinate',
            ];
        }

        return $methods;
    }


    public function deserializeCoordinate(VisitorInterface $visitor, $data, array $type, Context $context)
    {
        $type['name'] = 'float';

        return is_float($data) ? $data : null;
    }
}